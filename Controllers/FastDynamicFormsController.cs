﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FastDynamicForms.Core.Models;
using FastDynamicForms.ViewModels;
using FastDynamicForms.Utilities;

namespace FastDynamicForms.Controllers
{
    public class FastDynamicFormsController : Controller
    {
        private readonly DbContext context;

        public FastDynamicFormsController(DbContext context)
        {
            this.context = context;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Save(object model)
        {
            if (ModelState.IsValid)
            {
                EntityUtilities.SaveOrUpdateEntity(model, context);
            }
            else
            {
                EditViewModel viewModel = EntityUtilities.GetEditViewModel(model, context);
                return View("Edit", viewModel);
            }

            return RedirectToAction("List", new { modelTypeName = model.GetType().Name });
        }

        public IActionResult Create(string modelTypeName)
        {
            EditViewModel model = EntityUtilities.GetEmptyEditViewModel(modelTypeName, context);
            return View("Edit", model);
        }

        public IActionResult Edit(int id, string modelTypeName)
        {
            object entity = EntityUtilities.GetEntity(context, id, modelTypeName);
            EditViewModel model = EntityUtilities.GetEditViewModel(entity, context);

            return View(model);
        }

        //todo move logic out of controller
        public IActionResult List(string modelTypeName)
        {
            ModelsContainer container = Singleton<ModelsContainer>.Instance;

            var modelType = container.ModelTypes.Select(x => x.Key)
                .FirstOrDefault(x => x.Name == modelTypeName);
            if (modelType == null)
                throw new ArgumentException($"Registered model with name of \"{modelTypeName}\" not found");

            var viewModel = new RegisteredModelsViewModel()
            {
                Name = modelType.Name,
                EntityForDisplay = Activator.CreateInstance(modelType)
            };
            
            IQueryable dbSet = (IQueryable)EntityUtilities.GetEntityQuery(context, modelType);
            foreach (object dbSetItem in dbSet)
            {
                ListItemViewModel listItem = new ListItemViewModel(dbSetItem);
                viewModel.Items.Add(listItem);
            }
            viewModel.PropertyNames = modelType.GetProperties().Select(x => x.Name).ToList();

            return View(viewModel);
        }

        //todo move logic out of controller
        public IActionResult Index()
        {
            ModelsContainer container = Singleton<ModelsContainer>.Instance;
            var viewModels = new List<RegisteredModelsViewModel>();

            viewModels.AddRange(container.ModelTypes.Select(x => new RegisteredModelsViewModel()
            {
                Name = x.Key.Name
            }));

            return View(viewModels);
        }
    }
}
