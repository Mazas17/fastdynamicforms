﻿using Microsoft.Extensions.FileProviders;
using System;
using System.IO;
using System.Reflection;

namespace FastDynamicForms.Core.FileProviders
{
    /// <summary>
    /// Build cshtml views to resource(settings for building in project file) in order to access them from dll
    /// </summary>
    public class ResourceFileInfo : IFileInfo
    {
        private bool exists;
        private Stream resourceStream;

        public ResourceFileInfo(string subpath)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string projectName = assembly.GetName().Name;
            subpath = projectName + subpath.Replace("/", ".");
            resourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(subpath);
            exists = resourceStream != null;
        }
        public bool Exists => exists;

        public long Length
        {
            get
            {
                return resourceStream.Length;
            }
        }

        public string PhysicalPath => null;

        public string Name => throw new NotImplementedException();

        public DateTimeOffset LastModified => throw new NotImplementedException();

        public bool IsDirectory => false;

        public Stream CreateReadStream()
        {
            return resourceStream;
        }
    }
}
