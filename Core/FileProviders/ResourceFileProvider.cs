﻿using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Primitives;

namespace FastDynamicForms.Core.FileProviders
{
    public class ResourceFileProvider : IFileProvider
    {
        public IDirectoryContents GetDirectoryContents(string subpath)
        {
            return new ResourceDirectoryContents();
        }

        public IFileInfo GetFileInfo(string subpath)
        {
            return new ResourceFileInfo(subpath);
        }

        public IChangeToken Watch(string filter)
        {
            return new ResourceChangeToken();
        }
    }
}
