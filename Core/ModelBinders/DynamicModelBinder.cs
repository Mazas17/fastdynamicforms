﻿using FastDynamicForms.Core.Models;
using FastDynamicForms.Extensions;
using FastDynamicForms.Utilities;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace FastDynamicForms.Core.ModelBinders
{
    class DynamicModelBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            //sadly we need context here to bind navigational properties
            //maybe improve so we bind only FK and get rid of context here
            DbContext context = (DbContext)bindingContext.HttpContext.RequestServices
                .GetService(Singleton<DbContextContainer>.Instance.DbContextType);

            var modelTypeName = bindingContext.ValueProvider.GetValue("ModelTypeName");
            var modelType = Singleton<ModelsContainer>.Instance.ModelTypes
                .FirstOrDefault(x => x.Key.Name == modelTypeName.FirstValue);

            if (modelType.Key != null)
            {
                object entityInstance = Activator.CreateInstance(modelType.Key);

                //binding all properties to model
                foreach (PropertyInfo property in modelType.Key.GetProperties())
                {
                    string propertyName = property.Name;
                    ValueProviderResult boundValue = bindingContext.ValueProvider.GetValue(propertyName);

                    if (property.IsNavigationalProperty())
                    {
                        object convertedValue = null;
                        if (!string.IsNullOrEmpty(boundValue.FirstValue))
                        {
                            //get navigational property PK
                            PropertyInfo primaryKeyPropertyInfo = property.PropertyType.GetProperties()
                                .First(x => x.Name == EntityUtilities.GetPrimaryKeyPropertyName(property.PropertyType));
                            //convert to exact value
                            convertedValue = Convert.ChangeType(boundValue.FirstValue, primaryKeyPropertyInfo.PropertyType);
                        }

                        //find FK entity from DB and set it to entity
                        object foreignKeyEntity = EntityUtilities.GetEntity(context, convertedValue, property.PropertyType);
                        property.SetValue(entityInstance, foreignKeyEntity);
                    }
                    //simply bind value
                    else
                    {
                        object convertedValue = null;
                        if (!string.IsNullOrEmpty(boundValue.FirstValue))
                        {
                            convertedValue = Convert.ChangeType(boundValue.FirstValue, property.PropertyType);
                        }

                        property.SetValue(entityInstance, convertedValue);
                    }
                }

                bindingContext.Result = ModelBindingResult.Success(entityInstance);
            }
            else
            {
                bindingContext.Result = ModelBindingResult.Failed();
            }
            
            return Task.CompletedTask;
        }
    }
}
