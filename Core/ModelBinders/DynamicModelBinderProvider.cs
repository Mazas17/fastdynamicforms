﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;

namespace FastDynamicForms.Core.ModelBinders
{
    public class DynamicModelBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            //todo it may not be safe
            //todo check if assembly is same
            return context.Metadata.ModelType == typeof(object) ? new DynamicModelBinder() : null;
        }
    }
}
