﻿using System;
using System.Collections.Generic;

namespace FastDynamicForms.Core.Models
{
    class ModelsContainer
    {
        /// <summary>
        /// Key is model type. Value is model type admin class.
        /// </summary>
        public Dictionary<Type, Type> ModelTypes { get; set; } = new Dictionary<Type, Type>();
    }
}
