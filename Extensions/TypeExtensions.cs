﻿using FastDynamicForms.Utilities;
using System;
using System.Collections;
using System.Linq;
using System.Reflection;

namespace FastDynamicForms.Extensions
{
    public static class TypeExtensions
    {
        internal static object GetDefaultValue(this Type type)
        {
            return type.IsValueType ? Activator.CreateInstance(type) : null;
        }

        public static bool IsNavigationalProperty(this PropertyInfo propertyInfo)
        {
            return (propertyInfo.PropertyType.IsClass && !propertyInfo.PropertyType.GetInterfaces().Contains(typeof(IEnumerable)))
                && propertyInfo.PropertyType != typeof(string);
        }

        public static bool IsPrimaryKey(this PropertyInfo propertyInfo)
        {
            return propertyInfo.Name == EntityUtilities.GetPrimaryKeyPropertyName(propertyInfo.PropertyType);
        }

        internal static bool IsOverriden(this MethodInfo methodInfo)
        {
            return methodInfo.GetBaseDefinition().DeclaringType != methodInfo.DeclaringType;
        }
    }
}
