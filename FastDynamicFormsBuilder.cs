﻿using System;
using FastDynamicForms.Core.FileProviders;
using FastDynamicForms.Core.ModelBinders;
using FastDynamicForms.Core.Models;
using FastDynamicForms.Controllers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using FastDynamicForms.Core;

namespace FastDynamicForms
{
    public static class FastDynamicFormsBuilder
    {
        private static ModelsContainer container = Singleton<ModelsContainer>.Instance;

        /// <summary>
        /// Registers model type for rendering dynamic html form
        /// </summary>
        /// <param name="type">Type of model to register</param>
        public static void RegisterModelType(Type type)
        {
            container.ModelTypes.Add(type, null);
        }

        //adminType is not implemented yet
        //public static void RegisterModelTypeWithAdmin(Type modelType, Type adminType)
        //{
        //    //container.ModelTypes.Add(modelType, adminType);
        //    throw new NotImplementedException();
        //}

        /// <summary>
        /// Adds to services FastDynamicFormsController, adds new ResourceFileProvider and DynamicModelBinderProvider
        /// </summary>
        public static IServiceCollection ConfigureFastDynamicFormServices(this IServiceCollection services)
        {
            //add controller as service, so it will be seen in other assembly
            services.AddTransient<FastDynamicFormsController, FastDynamicFormsController>();

            services.Configure<RazorViewEngineOptions>(options =>
            {
                options.FileProviders.Add(new ResourceFileProvider());
            });

            services.AddMvc(options =>
            {
                options.ModelBinderProviders.Insert(0, new DynamicModelBinderProvider());
            });

            //when compiled to dll, this is required for executing project to detect this assembly
            services.AddMvc().AddApplicationPart(typeof(FastDynamicFormsBuilder).Assembly);
            return services;
        }

        /// <summary>
        /// Registers database context class as scoped service to the specified IServiceCollection
        /// </summary>
        /// <param name="dbContextType">Type of database context class</param>
        public static IServiceCollection RegisterDbContext(this IServiceCollection services, Type dbContextType)
        {
            services.AddScoped(typeof(DbContext), dbContextType);
            Singleton<DbContextContainer>.Instance.DbContextType = dbContextType;
            return services;
        }

        /// <summary>
        /// Adds new route
        /// </summary>
        public static void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "FDF",
                    template: "{controller=FastDynamicForms}/{action=Index}");
            });
        }
    }
}
