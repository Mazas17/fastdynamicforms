# Fast Dynamic Forms #

Create dynamic html forms with couple lines of code

# Requirements #

This library requires ASP.NET Core 2.0.2 version

# Installation #

You can either download the source and build your own dll ~~or, if you have the NuGet package manager installed, you can grab them automatically.~~

# Getting started #

Add the following namespace in `Startup.cs` file to use the library:

    using FastDynamicForms;

In order to make dynamic forms available, you must have your database context class, derived from `DbContext`. Register context class type as shown below:

	public void ConfigureServices(IServiceCollection services)
    {
        ...
        FastDynamicFormsBuilder.RegisterDbContext(services, typeof(MyCustomDbContext)).ConfigureFastDynamicFormServices();
    }
    
Also add the following line in `Configure` method:
    
    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
        ....
        FastDynamicFormsBuilder.Configure(app, env);
    }
    
Register your model class types as shown below.

    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
        ....
        FastDynamicFormsBuilder.Configure(app, env);
        
        FastDynamicFormsBuilder.RegisterModelType(typeof(Book));
        FastDynamicFormsBuilder.RegisterModelType(typeof(Author));
    }

Make sure that your context class has `DbSet` properties. Example:

    public partial class MyCustomDbContext : DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Author> Authors { get; set; }
    }
    
**That's all!** Now run your web application and visit `localhost:12345/FastDynamicForms`