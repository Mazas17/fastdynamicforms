﻿using FastDynamicForms.Core.Models;
using FastDynamicForms.Extensions;
using FastDynamicForms.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace FastDynamicForms.Utilities
{
    internal static class EntityUtilities
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model">New or updated model</param>
        internal static void SaveOrUpdateEntity(object model, DbContext context)
        {
            Type entityType = model.GetType();
            PropertyInfo idPropertyInfo = entityType.GetProperty(EntityUtilities.GetPrimaryKeyPropertyName(entityType));
            object idValue = idPropertyInfo.GetValue(model);

            //entity is new
            if (idValue == null || idValue.Equals(idPropertyInfo.PropertyType.GetDefaultValue()))
            {
                context.Add(model);

                //do not create new FK entity
                foreach (EntityEntry trackerEntry in context.ChangeTracker.Entries().Where(x => x.Entity.GetType() != entityType))
                {
                    trackerEntry.State = EntityState.Unchanged;
                }
                    
                context.SaveChanges();
            }
            else
            {
                object entity = GetEntity(context, idValue, entityType);

                //ignore primary keys and backward(List) navigational properties
                foreach (PropertyInfo property in entityType.GetProperties().Where(x => (x.Name != EntityUtilities.GetPrimaryKeyPropertyName(entityType)
                    && !x.PropertyType.GetInterfaces().Contains(typeof(IEnumerable))) || x.PropertyType == typeof(string)))
                {
                    //model value(from html form)
                    object modelValue = entityType.GetProperty(property.Name).GetValue(model);
                    //changing entity value to model value
                    entityType.GetProperty(property.Name).SetValue(entity, modelValue);

                    //if we changed navigational property (FK), EF tracks it as new entity
                    if (modelValue != null && property.IsNavigationalProperty())
                    {
                        //actually it is not new, so we don't create new entity, instead just update FK relations
                        context.Entry(modelValue).State = EntityState.Unchanged;
                    }
                }
                
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Gets empty EditViewModel for new entity
        /// </summary>
        /// <param name="entityTypeName"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        internal static EditViewModel GetEmptyEditViewModel(string entityTypeName, DbContext context)
        {
            EditViewModel model = new EditViewModel();
            Type modelType = Singleton<ModelsContainer>.Instance.ModelTypes
                .FirstOrDefault(x => x.Key.Name == entityTypeName).Key;//todo: fix possible NullReference

            //used just for entity context
            object entity = Activator.CreateInstance(modelType);

            return GetEditViewModel(entity, context);
        }

        /// <summary>
        /// Gets EditViewModel filled with data from entity
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        internal static EditViewModel GetEditViewModel(object entity, DbContext context)
        {
            EditViewModel model = new EditViewModel();
            string entityTypeName = entity.GetType().Name;

            Type modelType = Singleton<ModelsContainer>.Instance.ModelTypes
                .FirstOrDefault(x => x.Key.Name == entityTypeName).Key;//todo: fix possible NullReference
            if (modelType != null)
            {
                model.Name = modelType.Name;
                
                foreach (var property in modelType.GetProperties())
                {
                    if (property.IsNavigationalProperty())
                    {
                        var selectItems = new List<SelectListItem>();

                        //get all related entities
                        var relatedEntities = GetEntityQuery(context, property.PropertyType);
                        //current (main) entity id value
                        object entityIdValue = entity.GetType().GetProperty(GetPrimaryKeyPropertyName(entity.GetType())).GetValue(entity);
                        foreach (object relatedEntity in (IQueryable)relatedEntities)
                        {
                            Type entityType = relatedEntity.GetType();
                            MethodInfo toStringMethod = entityType.GetMethod("ToString");

                            //navigational property (FK) id value
                            object idValue = entityType.GetProperty(GetPrimaryKeyPropertyName(entityType)).GetValue(relatedEntity);
                            //for better user experience take ToString value, if it's overriden
                            //else just display PK value
                            string displayText = toStringMethod.IsOverriden() ? toStringMethod.Invoke(relatedEntity, new object[] { }).ToString()
                                : idValue.ToString();

                            selectItems.Add(new SelectListItem()
                            {
                                Text = displayText,
                                Value = idValue.ToString(),
                                Selected = entityIdValue.Equals(idValue)
                            });
                        }
                        //default value for null FK
                        selectItems.Insert(0, new SelectListItem()
                        {
                            Text = "-",
                            Value = 0.ToString()
                        });

                        model.Properties.Add(property.Name, selectItems);
                    }
                    //if it's not navigaional property, simply add value
                    else
                    {
                        object value = property.GetValue(entity);
                        model.Properties.Add(property.Name, value);
                    }

                }

                model.Entity = entity;
            }

            return model;
        }
        
        internal static object GetEntityQuery(DbContext context, Type entityType)
        {
            if (entityType == null)
                throw new ArgumentNullException(nameof(entityType));

            Type dbContextType = context.GetType();
            //invoke set method with generic type of entityType
            object dbSet = dbContextType.GetMethod("Set")
                .MakeGenericMethod(entityType)
                .Invoke(context, null);
            
            var navigationalProperties = entityType.GetProperties()
                .Where(prop => prop.IsNavigationalProperty())
                .Select(s => s.Name);//todo check all types

            MethodInfo includeMethod = typeof(EntityFrameworkQueryableExtensions)
                .GetMethods(BindingFlags.Static | BindingFlags.Public)
                .First(m => m.Name == "Include" && m.GetParameters().Any(y => y.ParameterType == typeof(string)));

            //invoking include for each navigational property (FK)
            //necessary since ASP.NET Core doesn't support lazy loading
            foreach (string property in navigationalProperties)
            {
                dbSet = includeMethod.MakeGenericMethod(entityType)
                    .Invoke(null, new object[] { dbSet, property });
            }

            return dbSet;
        }

        internal static object GetEntity(DbContext context, object id, string entityTypeName)
        {
            Type modelType = Singleton<ModelsContainer>.Instance.ModelTypes
                .FirstOrDefault(x => x.Key.Name == entityTypeName).Key;//todo: fix possible NullReference

            return GetEntity(context, id, modelType);
        }

        /// <summary>
        /// Gets single entity with all navigational properties included
        /// </summary>
        /// <param name="context"></param>
        /// <param name="id"></param>
        /// <param name="entityType"></param>
        /// <returns></returns>
        internal static object GetEntity(DbContext context, object id, Type entityType)
        {
            object query = GetEntityQuery(context, entityType);

            //dynamically creating and invoking FirstOrDefault method on query to get single entity
            ParameterExpression param = Expression.Parameter(entityType, "x");
            Expression idProperty = Expression.Property(param, GetPrimaryKeyPropertyName(entityType));
            Expression idValue = Expression.Constant(id);
            Expression findExpression = Expression.Equal(idProperty, idValue);

            //Expression.Lambda<Func<entityType, bool>>(findExpression, param)
            MethodInfo lambdaExpression = typeof(Expression)
                .GetMethods(BindingFlags.Public | BindingFlags.Static)
                .First(x => x.Name == "Lambda" && x.IsGenericMethod
                    && x.GetParameters().Count() == 2 && x.GetParameters()[1].ParameterType == typeof(ParameterExpression[]));

            //Func<entityType, bool>
            Type funcType = typeof(Func<,>)
                .MakeGenericType(entityType, typeof(bool));

            //object invokedLambdaExpression = Expression.Lambda<Func<entityType, bool>>(mainExpr, param)
            object invokedLambdaExpression = lambdaExpression.MakeGenericMethod(funcType)
                .Invoke(findExpression, new object[] { findExpression, new ParameterExpression[] { param } });

            MethodInfo firstMethod = typeof(Queryable)
                .GetMethods(BindingFlags.Public | BindingFlags.Static)
                .First(x => x.Name == "FirstOrDefault" && x.GetParameters().Count() == 2);

            //return query.First(invokedLambdaExpression)
            return firstMethod.MakeGenericMethod(entityType)
                .Invoke(null, new object[] { query, invokedLambdaExpression });
        }

        internal static string GetPrimaryKeyPropertyName(Type entityType)
        {
            return "Id";
        }
    }
}
