﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FastDynamicForms.ViewModels
{
    public class EditViewModel
    {
        public string Name { get; set; }

        public object Entity { get; set; }

        /// <summary>
        /// Key: property name
        /// Value: property value
        /// </summary>
        public Dictionary<string, object> Properties { get; set; } = new Dictionary<string, object>();
    }
}
