﻿using System.Collections.Generic;
using System.Reflection;

namespace FastDynamicForms.ViewModels
{
    public class ListItemViewModel
    {
        private readonly object model;

        public ListItemViewModel(object model)
        {
            this.model = model;
        }

        /// <summary>
        /// Enumerates model properties returning KeyValuePair with key of proprty info and value of property value
        /// </summary>
        public IEnumerator<KeyValuePair<PropertyInfo, object>> GetEnumerator()
        {
            foreach (PropertyInfo propertyInfo in model.GetType().GetProperties())
            {
                object value = propertyInfo.GetValue(model);
                yield return new KeyValuePair<PropertyInfo, object>(propertyInfo, value);
            }
        }
    }
}
