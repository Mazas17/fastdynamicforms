﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FastDynamicForms.ViewModels
{
    public class ListItemsViewModel
    {
        public List<object> PropertyValues { get; set; } = new List<object>();
    }
}
