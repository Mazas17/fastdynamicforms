﻿using System.Collections.Generic;

namespace FastDynamicForms.ViewModels
{
    public class RegisteredModelsViewModel 
    {
        public string Name { get; set; }
        public List<string> PropertyNames { get; set; } = new List<string>();
        public List<ListItemViewModel> Items { get; set; } = new List<ListItemViewModel>();

        /// <summary>
        /// Should be empty instance. Used for display attributes
        /// </summary>
        public object EntityForDisplay { get; set; }
    }
}
